"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const AWS = __importStar(require("aws-sdk"));
const joi_1 = __importDefault(require("@hapi/joi"));
// Schema for plug-in properties
const schema = joi_1.default.object({
    accessKeyId: joi_1.default.string().alphanum().required(),
    secretAccessKey: joi_1.default.string().alphanum().required(),
    region: joi_1.default.string().required()
}).unknown();
const sendEmail = async function (params) {
    return await new AWS.SES({ apiVersion: '2010-12-01' }).sendEmail(params).promise();
};
const sendText = async function (phoneNumber, message) {
    const params = {
        Message: message,
        PhoneNumber: phoneNumber
    };
    return await new AWS.SNS({ apiVersion: '2010-03-31' }).publish(params).promise();
};
const uploadToS3 = async function (params) {
    return await new AWS.S3().upload(params).promise();
};
/**
 * register the plug-in with the Hapi framework
 *
 * @param  {Object} plugin
 * @param  {Object} options
 * @param  {Function} next
 */
exports.plugin = {
    name: 'hapi-aws',
    version: '1.1.1',
    register: function (server, options) {
        joi_1.default.assert(options, schema);
        AWS.config.update(options);
        server.method('sendEmail', sendEmail);
        server.method('sendText', sendText);
        server.method('uploadToS3', uploadToS3);
    },
};
//# sourceMappingURL=index.js.map