import { Plugin } from '@hapi/hapi';

declare module '@hapi/hapi' {
  // https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/hapi__hapi/index.d.ts#L97
  interface PluginSpecificConfiguration {
        'hapi-aws'?: {
            accessKeyId: string,
            secretAccessKey: string,
            region: string
        }
    }
}

declare namespace hapiaws {
    interface RegisterOptions {
        accessKeyId: string,
        secretAccessKey: string,
        region: string
    }
}

declare const hapiaws: Plugin<hapiaws.RegisterOptions>;

export = hapiaws;
