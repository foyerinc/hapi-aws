import * as AWS from 'aws-sdk';
import Joi from '@hapi/joi';
import { Plugin, Server } from '@hapi/hapi';
import { SendEmailRequest, SendEmailResponse } from 'aws-sdk/clients/ses';
import { PublishInput, PublishResponse } from 'aws-sdk/clients/sns';
import { PutObjectRequest, ManagedUpload } from 'aws-sdk/clients/s3';

// Schema for plug-in properties
const schema = Joi.object({
    accessKeyId: Joi.string().alphanum().required(),
    secretAccessKey: Joi.string().alphanum().required(),
    region: Joi.string().required()
}).unknown();

interface RegisterOptions {
    accessKeyId: string,
    secretAccessKey: string,
    region: string
}



const sendEmail = async function (params: SendEmailRequest): Promise<SendEmailResponse> {
    return await new AWS.SES({ apiVersion: '2010-12-01' }).sendEmail(params).promise();
}

const sendText = async function (phoneNumber: string, message: string): Promise<PublishResponse> {
    const params: PublishInput = {
        Message: message,
        PhoneNumber: phoneNumber
    };

    return await new AWS.SNS( { apiVersion: '2010-03-31' } ).publish(params).promise();
}

const uploadToS3 = async function (params: PutObjectRequest): Promise<ManagedUpload.SendData> {
    return await new AWS.S3().upload(params).promise();
};

/**
 * register the plug-in with the Hapi framework
 *
 * @param  {Object} plugin
 * @param  {Object} options
 * @param  {Function} next
 */
export const plugin: Plugin<RegisterOptions> = {
    name: 'hapi-aws',
    version: '1.1.1',
    register: function(server: Server, options: RegisterOptions) {
        Joi.assert(options, schema);
        
        AWS.config.update(options);
        server.method('sendEmail', sendEmail);
        server.method('sendText', sendText);
        server.method('uploadToS3', uploadToS3);
    },
};